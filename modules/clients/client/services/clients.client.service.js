(function () {
  'use strict';

  function ClientsAPI(API_URL, $resource) {
    return $resource(API_URL + '/client/:claveCte', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('ventas').factory('ClientsAPI', [
    'API_URL',
    '$resource',
    ClientsAPI
  ]);
}());