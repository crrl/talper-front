(function () {
  'use strict';

  function configAPI(API_URL, $resource) {
    return $resource(API_URL + '/config/:id', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('ventas').factory('configAPI', [
    'API_URL',
    '$resource',
    configAPI
  ]);
}());
