(function () {
  'use strict';

  function topbar($rootScope) {
    function link() {
      $('.dropdown-button').dropdown({
        hover: false,
        gutter: 0
      });
    }
    return {
      restrict: 'E',
      templateUrl: 'modules/core/client/views/header.client.view.html',
      link: link
    }
  }

  angular.module('core').directive('topbar', [
    topbar
  ]);
}());