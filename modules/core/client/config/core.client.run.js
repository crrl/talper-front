(function () {
  'use strict';

  angular.module('core').run(function ($rootScope, $state) {
    $rootScope.$on('$stateChangeSuccess', function (event, toState) {
      $rootScope.topbarCurrent = 'Fecha: ' + new Date().toJSON().slice(0, 10).split('-').reverse().join('/');
    });
  });
}());
