(function () {
  'use strict';

  function HeaderController($scope, $rootScope, $state) {
  }

  angular.module('core').controller('headerController', [
    '$scope',
    '$rootScope',
    '$state',
    HeaderController
  ]);
}());

