(function () {
  'use strict';
  function SellsAPI(API_URL, $resource) {
    return $resource(API_URL + '/activeSells/:id', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('ventas').factory('SellsAPI', [
    'API_URL',
    '$resource',
    SellsAPI
  ]);
}());
