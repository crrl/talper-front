(function () {
  'use strict';

  angular
  .module('ventas')
  .config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
    .state('sells', {
      url: '/ventas',
      parent: 'root',
      templateUrl: '/modules/ventas/client/views/sells.client.view.html',
      controller: 'sellsCtrl'
    })
    .state('new-sell', {
      url: '/nueva-venta',
      parent: 'root',
      templateUrl: '/modules/ventas/client/views/new-sell.client.view.html',
      controller: 'newSellCtrl'
    });
  }
}());
