(function () {
  'use strict';

  function newSellCtrl($scope, $state, SellsAPI, ClientsAPI, articlesAPI, configAPI) {

    $scope.currentSoldItems = [];
    $scope.enganche = 0;
    $scope.bonificacion = 0;
    $scope.totalAdeudo = 0;
    $scope.importe = 0;
    $scope.contado = 0;
    $scope.text = 'Continuar';
    $scope.months = [{
      month: 3,
      selected: false
    }, {
      month: 6,
      selected: false
    }, {
      month: 9,
      selected: false
    }, {
      month: 12,
      selected: false
    }];
    $scope.$watch('currentSoldItems', function (newValue, oldValue) {
      $scope.enganche = 0;
      $scope.bonificacion = 0;
      $scope.importe = 0;
      newValue.forEach(function (item) {
        $scope.enganche += item.realPrice * item.cantidad;
      });
      if ($scope.config) {
        $scope.importe = angular.copy($scope.enganche);
        $scope.enganche = $scope.enganche * ($scope.config.porcEnganche / 100);
        $scope.bonificacion = $scope.enganche * (($scope.config.tasa * $scope.config.plazoMaximo)/100);
        $scope.totalAdeudo = $scope.importe - $scope.enganche - $scope.bonificacion;
        $scope.contado = $scope.totalAdeudo / (1 + (($scope.config.tasa * $scope.config.plazoMaximo)/100));
      }
    }, true);

    function initialInfo() {

      configAPI.query().$promise
      .then(function (res) {
        $scope.config = res[0];
      });

      ClientsAPI.query().$promise
      .then(function (res) {
        $scope.clients = res;
        $scope.clients.forEach(function (client, index) {
          client.fullName = client.nombre + ' ' + client.apPaterno + ' ' + client.apMaterno;
          client.showName = client.claveCte + ' - ' + client.fullName;
        });
      });

      articlesAPI.query().$promise
      .then(function (res) {
        $scope.articles = res;
      });

      SellsAPI.query().$promise
      .then(function (res) {
        $scope.step = 1;
        $scope.text = 'Continuar';
        $scope.folio = res.length + 1;
        $scope.folioToShow = JSON.stringify($scope.folio);
        while ($scope.folioToShow.length < 4) {
          $scope.folioToShow = '0' + $scope.folioToShow;
        }
      })
      .catch(function (err) {
        Materialize.toast('Ha ocurrido un error.', 2000);
      });
    }


    $scope.addItem = function () {
      var prevent = false;

      if (!$scope.selectedArticle || !$scope.selectedArticle.originalObject) {
        return;
      }

      $scope.currentSoldItems.forEach(function (item) {
        if (item.claveArticulo === $scope.selectedArticle.originalObject.claveArticulo) {
          prevent = true;
          return;
        }
      });
      if (prevent) {
        return;
      }

      if ($scope.selectedArticle.originalObject.existencia <= 0) {
        Materialize.toast('El artículo seleccionado no cuenta con existencia, favor de verificar.', 2000);
        return;
      } else {
        $scope.selectedArticle.originalObject.realPrice = $scope.selectedArticle.originalObject.precio * (1 + ($scope.config.tasa * $scope.config.plazoMaximo) / 100);
        $scope.currentSoldItems.push($scope.selectedArticle.originalObject);
      }
    };


    $scope.removeSelectedSell = function (index) {
      $scope.currentSoldItems.splice(index, 1);
      $scope.step = 1;
      $scope.text = 'Continuar';
    };

    $scope.verifyMax = function (index) {
      $scope.enganche = 0;
      if ($scope.currentSoldItems[index].cantidad > $scope.currentSoldItems[index].existencia) {
        $scope.currentSoldItems[index].cantidad = $scope.currentSoldItems[index].existencia;
      }
      $scope.currentSoldItems.forEach(function (item) {
        $scope.enganche += item.realPrice * item.cantidad;
      });
      if ($scope.config) {
        $scope.importe = angular.copy($scope.enganche);
        $scope.enganche = $scope.enganche * ($scope.config.porcEnganche / 100);
        $scope.bonificacion = $scope.enganche * (($scope.config.tasa * $scope.config.plazoMaximo)/100);
        $scope.totalAdeudo = $scope.importe - $scope.enganche - $scope.bonificacion;
        $scope.contado = $scope.totalAdeudo / (1 + (($scope.config.tasa * $scope.config.plazoMaximo)/100));
      }
    };

    $scope.nextStep = function (step) { 
      var prevent = false;
      if ( step === 1) {
        if ($scope.currentSoldItems.length === 0) {
          prevent = true;
        }

        $scope.currentSoldItems.forEach(function (sold) {
          if (sold.cantidad <= 0) {
            prevent = true;
          }
        });

        if (!$scope.selectedClient || !$scope.selectedClient.originalObject) {
          prevent = true;
        }

        if (prevent) {
          Materialize.toast('Los datos ingresados no son correctos, favor de verificar', 2000);
        } else {
          $scope.step = 2;
          $scope.text = 'Guardar';
          $scope.months = [];
          for (let plazo = 1; plazo <= ($scope.config.plazoMaximo / 3); plazo++) {
            $scope.months.push({
              month: plazo * 3,
              selected: false
            });
          }
        }
      } else {
        if (!$scope.selectedMonth) {
          Materialize.toast('Debe seleccionar un plazo para realizar el pago de su compra', 2000);
          return;
        }

        $scope.currentSoldItems.forEach(function (sold) {
          if (sold.cantidad <= 0) {
            Materialize.toast('Los datos ingresados no son correctos, favor de verificar', 2000);
            return;
          }
        });

        if (!$scope.selectedClient || !$scope.selectedClient.originalObject) {
          Materialize.toast('Los datos ingresados no son correctos, favor de verificar', 2000);
          return;
        }

        $scope.date = new Date().toJSON().slice(0, 10).split('-').reverse().join('/');
        $scope.totalSell = $scope.contado * (1 + ($scope.config.tasa * $scope.selectedMonth)/100);
        SellsAPI.save({
          folioVenta: $scope.folioToShow,
          claveCliente: $scope.selectedClient.originalObject.claveCte,
          nombre: $scope.selectedClient.originalObject.fullName,
          total: $scope.totalSell,
          fecha: $scope.date,
          articulos: $scope.currentSoldItems
        }).$promise
        .then(function (res) {
          Materialize.toast('Bien Hecho, Tu venta ha sido registrada correctamente', 2000);
          $state.go('sells');
        })
        .catch(function (err) {
          Materialize.toast('Ha ocurrido un error en el servidor.', 2000);
        });
      }
    };

    $scope.setSelectedRadio = function (index) {
      $scope.months.forEach(function (month) {
        month.selected = false;
      });
      $scope.months[index].selected = true;
      $scope.selectedMonth = angular.copy($scope.months[index].month);
    };


    $scope.cancelModal = function () {
      angular.element('#cancel-new-sell').modal();
      angular.element('#cancel-new-sell').modal('open');
    };

    $scope.cancelTransaction = function () {
      angular.element('#cancel-new-sell').modal('close');
      $state.go('sells');
    };

    initialInfo();
  }

  angular.module('ventas').controller('newSellCtrl', [
    '$scope',
    '$state',
    'SellsAPI',
    'ClientsAPI',
    'articlesAPI',
    'configAPI',
    newSellCtrl
  ]);
}());
