(function () {
  'use strict';

  function sellsCtrl($scope, $rootScope, SellsAPI) {
    $rootScope.topbarCurrent = 'Fecha: ' + new Date().toJSON().slice(0, 10).split('-').reverse().join('/');

    SellsAPI.query().$promise
    .then(function (res) {
      $scope.items = res;
    })
    .catch(function (err) {
      Materialize.toast('Ha ocurrido un error.', 2000);
    });
  }

  angular.module('ventas').controller('sellsCtrl', [
    '$scope',
    '$rootScope',
    'SellsAPI',
    sellsCtrl
  ]);
}());
