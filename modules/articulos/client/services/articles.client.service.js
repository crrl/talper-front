(function () {
  'use strict';
  function articlesAPI(API_URL, $resource) {
    return $resource(API_URL + '/article/:id', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

  angular.module('ventas').factory('articlesAPI', [
    'API_URL',
    '$resource',
    articlesAPI
  ]);
}());
